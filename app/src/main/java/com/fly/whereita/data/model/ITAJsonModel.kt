package com.fly.whereita.data.model


data class ITAJsonModel(
    val id: Long?,
    val continent: String,
    val iata: String,
    val iso: String,
    val lat: String?,
    val lon: String?,
    val name: String?,
    val size: String?,
    val status: Int,
    val type: String)