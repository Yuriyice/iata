package com.fly.whereita.data.network.core

import com.fly.whereita.data.network.Api
import com.fly.whereita.data.network.utils.ServerDate
import com.fly.whereita.data.network.utils.ServerDateDeserializer
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Yuriy Aizenberg on 25.04.2021.
 */
object NetworkClientBuilder {

    private const val READ_WRITE_TIMEOUT = 30L

    fun getClient() : OkHttpClient {
        return OkHttpClient.Builder()
            .readTimeout(READ_WRITE_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(READ_WRITE_TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(READ_WRITE_TIMEOUT, TimeUnit.SECONDS)
            .build()
    }

    fun getRetrofit(client: OkHttpClient, url: String, gson: Gson) : Retrofit {
        return Retrofit.Builder()
            .baseUrl(url)
            .validateEagerly(true)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .client(client)
            .build()
    }

    fun getApi(retrofit: Retrofit) = retrofit.create(Api::class.java)

    fun getGson() : Gson {
        return GsonBuilder()
            .registerTypeAdapter(ServerDate::class.java, ServerDateDeserializer())
            .create()
    }


}