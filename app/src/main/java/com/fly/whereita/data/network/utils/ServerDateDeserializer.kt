package com.fly.whereita.data.network.utils

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Yuriy Aizenberg on 26.04.2021.
 */
class ServerDateDeserializer : JsonDeserializer<ServerDate> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): ServerDate? {
        val dateFormatter = SimpleDateFormat(DATE_PATTERN, Locale.getDefault())
        return try {
            if (json != null) {
                val dateInString = json.asString
                val date = dateFormatter.parse(dateInString)
                if (date != null) {
                    ServerDate(date)
                } else {
                    null
                }
            } else {
                null
            }
        } catch (e: Exception) {
            null
        }
    }

    companion object {
        private const val DATE_PATTERN = "yyyy-MM-dd"
    }
}