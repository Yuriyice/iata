package com.fly.whereita.data.network.response

import com.fly.whereita.domain.model.Coordinates
import com.fly.whereita.domain.model.DirectionModel
import com.google.gson.annotations.SerializedName

/**
 * Created by Yuriy Aizenberg on 25.04.2021.
 */
class DirectionResponse(
    @SerializedName("direct")
    private val isDirect: Boolean,
    private val iata: String,
    private val name: String,
    private val country: String,
    @SerializedName("country_name")
    val countryName: String,
    private val coordinates: List<Double>?
) {

    fun toModel() = DirectionModel(
        isDirect = isDirect,
        iata = iata,
        name = name,
        country = country,
        countryName = countryName,
        coorindates = Coordinates.build(coordinates)
    )
}