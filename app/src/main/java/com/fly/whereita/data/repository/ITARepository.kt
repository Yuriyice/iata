package com.fly.whereita.data.repository

import com.fly.whereita.data.dao.ItaDao
import com.fly.whereita.data.datasource.IATANetworkDataSource
import com.fly.whereita.data.entities.IATAEntity
import com.fly.whereita.domain.model.IATADirectionsModel
import com.fly.whereita.domain.model.IATAModel
import com.fly.whereita.domain.model.PricesModel
import com.fly.whereita.domain.repository.IITARepository

/**
 * Created by Yuriy Aizenberg on 22.04.2021.
 */
class ITARepository(private val dao: ItaDao, private val dataSource: IATANetworkDataSource) :
    IITARepository {

    override suspend fun count(): Int {
        return dao.count()
    }

    override suspend fun all(): List<IATAModel> {
        return dao.findAll().map { it.toModel() }
    }

    override suspend fun insertAll(vararg model: IATAModel) {
        dao.insertAll(*model.map {
            IATAEntity(
                id = null,
                continent = it.continent,
                iata = it.iata,
                iso = it.iso,
                lat = it.lat,
                lon = it.lon,
                name = it.name,
                size = it.size,
                status = if (it.isOpened) 1 else 0,
                type = it.type
            )
        }.toTypedArray())
    }


    override suspend fun findByItaOrName(query: String, limit: Int): List<IATAModel> {
        return dao.findByItaOrName(query, limit).map { it.toModel() }
    }

    override suspend fun findSupportedDirectionsForIATA(iata: String): IATADirectionsModel {
        val findSupportedDirectionsForIATA = dataSource.findSupportedDirectionsForIATA(iata)
        return findSupportedDirectionsForIATA.toModel()
    }

    override suspend fun findPrices(iata: String): List<PricesModel> {
        return dataSource.findPrices(iata).map { it.toModel() }
    }
}