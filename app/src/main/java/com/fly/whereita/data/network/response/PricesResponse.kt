package com.fly.whereita.data.network.response


import com.fly.whereita.data.network.utils.ServerDate
import com.fly.whereita.domain.model.PricesModel
import com.google.gson.annotations.SerializedName

data class PricesResponse(
    @SerializedName("actual")
    val isActual: Boolean,
    val airline: String,
    @SerializedName("depart_date")
    val departDate: ServerDate?,
    val destination: String,
    val distance: Int,
    val gate: String,
    @SerializedName("number_of_changes")
    val numberOfChanges: Int,
    @SerializedName("return_date")
    val returnDate: ServerDate?,
    @SerializedName("show_to_affiliates")
    val showToAffiliates: Boolean,
    @SerializedName("trip_class")
    val tripClass: Int,
    val value: Double
) {
    fun toModel() = PricesModel(
        isActual = isActual,
        airline = airline,
        departDate = departDate?.date,
        destination = destination,
        distance = distance,
        gate = gate,
        numberOfChanges = numberOfChanges,
        returnDate = returnDate?.date,
        showToAffiliates = showToAffiliates,
        tripClass = PricesModel.TripClass.find(tripClass),
        value = value
    )
}