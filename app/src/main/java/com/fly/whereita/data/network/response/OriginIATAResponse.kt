package com.fly.whereita.data.network.response

import com.fly.whereita.domain.model.Coordinates
import com.fly.whereita.domain.model.OriginIATAModel

/**
 * Created by Yuriy Aizenberg on 25.04.2021.
 */
class OriginIATAResponse(
    private val iata: String,
    private val name: String,
    private val country: String,
    private val coordinates: List<Double>
) {
    fun toModel() = OriginIATAModel(
        iata = iata,
        name = name,
        country = country,
        coordinates = Coordinates.build(coordinates)
    )
}