package com.fly.whereita.data.datasource

import com.fly.whereita.data.network.Api

/**
 * Created by Yuriy Aizenberg on 25.04.2021.
 */
class IATANetworkDataSource(private val api: Api) {

    suspend fun findSupportedDirectionsForIATA(iataCode: String) = api.findSupportedDirectionsForIATA(iataCode)

    suspend fun findPrices(iata: String) = api.findPrices(iata)

}