package com.fly.whereita.data.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.fly.whereita.domain.model.IATAModel

/**
 * Created by Yuriy Aizenberg on 22.04.2021.
 */
@Entity(tableName = "iata_entity", indices = arrayOf(Index(value = ["iata", "name"])))
data class IATAEntity(
    @PrimaryKey(autoGenerate = true)
    val id: Long?,
    val continent: String,
    @ColumnInfo(name = "iata")
    val iata: String,
    val iso: String,
    val lat: Double?,
    val lon: Double?,
    @ColumnInfo(name = "name")
    val name: String,
    val size: String?,
    val status: Int,
    val type: String
) {

    fun toModel() = IATAModel(
        localId = id ?: -1,
        continent = continent,
        iata = iata,
        iso = iso,
        lat = lat,
        lon = lon,
        name = name,
        size = size,
        isOpened = status == 1,
        type = type
    )
}