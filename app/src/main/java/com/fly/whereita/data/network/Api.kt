package com.fly.whereita.data.network

import com.fly.whereita.data.network.response.IATADirectionsResponse
import com.fly.whereita.data.network.response.PricesResponse
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Yuriy Aizenberg on 25.04.2021.
 */
interface Api {

    @GET("/supported_directions.json")
    suspend fun findSupportedDirectionsForIATA(
        @Query("origin_iata") iataCode: String,
        @Query("one_way") oneWay: Boolean = true,
        @Query("locale") locale: String = "ru"
    ): IATADirectionsResponse

    @GET("/prices.json")
    suspend fun findPrices(
        @Query("origin_iata") iata: String,
        @Query("one_way") isOneWay: Boolean = false,
        @Query("locale") locale: String = "ru"
    ) : List<PricesResponse>

}