package com.fly.whereita.data.network.utils

import java.util.*

/**
 * Created by Yuriy Aizenberg on 26.04.2021.
 */
class ServerDate(val date: Date)