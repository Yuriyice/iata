package com.fly.whereita.data.network.response

import com.fly.whereita.domain.model.IATADirectionsModel

/**
 * Created by Yuriy Aizenberg on 25.04.2021.
 */
class IATADirectionsResponse(private val origin: OriginIATAResponse, private val directions: List<DirectionResponse>) {
    fun toModel() = IATADirectionsModel(origin = origin.toModel(), directions = directions.map { it.toModel() })
}