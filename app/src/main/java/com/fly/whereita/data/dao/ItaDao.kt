package com.fly.whereita.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.fly.whereita.data.entities.IATAEntity

/**
 * Created by Yuriy Aizenberg on 22.04.2021.
 */
@Dao
interface ItaDao {

    @Query("SELECT COUNT(id) FROM iata_entity")
    suspend fun count() : Int

    @Query("SELECT * FROM iata_entity")
    suspend fun findAll() : List<IATAEntity>

    @Insert
    @Transaction
    suspend fun insertAll(vararg itaEntity: IATAEntity)

    @Query("SELECT * FROM iata_entity WHERE iata LIKE '%' || :query || '%' OR name LIKE '%' || :query || '%' ORDER BY iata ASC LIMIT :limit")
    suspend fun findByItaOrName(query: String, limit: Int) : List<IATAEntity>

}