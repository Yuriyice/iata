package com.fly.whereita.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fly.whereita.domain.model.IATADirectionsModel
import com.fly.whereita.domain.repository.IITARepository
import com.fly.whereita.utils.SingleShotLiveData
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*

/**
 * Created by Yuriy Aizenberg on 25.04.2021.
 */
class IATADirectionsViewModel(private val repository: IITARepository) : ViewModel() {

    private val searchIATADirectionsLiveData = SingleShotLiveData<Outcome<IATADirectionsModel>>()
    val searchIATADirectionsEmitter: LiveData<Outcome<IATADirectionsModel>> =
        searchIATADirectionsLiveData

    fun searchDirectionsByIATACode(iata: String) {
        viewModelScope.launch {
            delay(800)
            try {
                val data = repository.findSupportedDirectionsForIATA(
                    iata.toUpperCase(
                        Locale.ENGLISH
                    )
                )
                val sortedList = data.directions.sortedWith(compareBy({ it.iata }, { it.name }))
                searchIATADirectionsLiveData.postValue(
                    Outcome.success(
                        IATADirectionsModel(data.origin, sortedList)
                    )
                )
            } catch (t: Throwable) {
                searchIATADirectionsLiveData.postValue(Outcome.failure(t))
            }
        }
    }


}