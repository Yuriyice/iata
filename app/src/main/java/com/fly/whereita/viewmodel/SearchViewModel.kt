package com.fly.whereita.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fly.whereita.domain.model.IATAModel
import com.fly.whereita.domain.repository.IITARepository
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

/**
 * Created by Yuriy Aizenberg on 24.04.2021.
 */
class SearchViewModel(private val repository: IITARepository) : ViewModel() {

    private var previousQueryJob: Job? = null
    private val searchItaLiveData = MutableLiveData<Outcome<List<IATAModel>>>()
    val searchItaEmitter: LiveData<Outcome<List<IATAModel>>> = searchItaLiveData

    fun findItaByNameSync(query: String, limit: Int): Outcome<SearchResultWrapper> {
        previousQueryJob?.cancel()
        if (query.isBlank()) {
            return Outcome.success(SearchResultWrapper("", listOf()))
        }
        var outcome: Outcome<SearchResultWrapper>? = null
        runBlocking {
            previousQueryJob = launch {
                outcome = try {
                    Outcome.success(
                        SearchResultWrapper(
                            query,
                            repository.findByItaOrName(query, limit)
                        )
                    )
                } catch (t: Throwable) {
                    Outcome.failure(t)
                }

            }
        }
        return outcome!!
    }

    fun findItaByName(query: String) {
        viewModelScope.launch {
            try {
                searchItaLiveData.postValue(Outcome.success(repository.findByItaOrName(query, Int.MAX_VALUE)))
            } catch (t: Throwable) {
                searchItaLiveData.postValue(Outcome.failure(t))
            }
        }
    }

    data class SearchResultWrapper(val originalQuery: String, val result: List<IATAModel>)
}