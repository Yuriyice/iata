package com.fly.whereita.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fly.whereita.domain.model.PricesModel
import com.fly.whereita.domain.repository.IITARepository
import com.fly.whereita.utils.SingleShotLiveData
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*

/**
 * Created by Yuriy Aizenberg on 26.04.2021.
 */
class IATAPricesViewModel(private val repository: IITARepository) : ViewModel() {

    private val pricesLiveData = SingleShotLiveData<Outcome<List<PricesModel>>>()
    val pricesEmitter: LiveData<Outcome<List<PricesModel>>> = pricesLiveData


    fun findPrices(iata: String) {
        viewModelScope.launch {
            delay(800)
            try {
                pricesLiveData.postValue(
                    Outcome.success(
                        repository.findPrices(
                            iata.toUpperCase(
                                Locale.ENGLISH
                            )
                        )
                    )
                )
            } catch (e: Exception) {
                pricesLiveData.postValue(Outcome.failure(e))
            }
        }
    }

}