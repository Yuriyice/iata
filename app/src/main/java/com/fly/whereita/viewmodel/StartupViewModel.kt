package com.fly.whereita.viewmodel

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fly.whereita.R
import com.fly.whereita.data.model.ITAJsonModel
import com.fly.whereita.domain.model.IATAModel
import com.fly.whereita.domain.repository.IITARepository
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.launch
import timber.log.Timber


/**
 * Created by Yuriy Aizenberg on 22.04.2021.
 */
class StartupViewModel(
    private val databaseWorkerDispatcher: CoroutineDispatcher,
    private val repository: IITARepository,
    private val parser: Gson
) : ViewModel() {

    private val databaseStateLiveData = MutableLiveData<DatabaseState>()
    val databaseStateEmitter : LiveData<DatabaseState> = databaseStateLiveData

    fun checkDatabaseState() {
        viewModelScope.launch(exceptionCoroutineContext) {
            val state = if (repository.count() == 0) {
                DatabaseState.EMPTY
            } else {
                DatabaseState.READY
            }
            databaseStateLiveData.postValue(state)
        }
    }


    fun populateDatabase(context: Context) {
        viewModelScope.launch(databaseWorkerDispatcher + exceptionCoroutineContext) {
            val json = context.resources.openRawResource(R.raw.ita_base).use { stream ->
                val bytes = ByteArray(stream.available())
                stream.read(bytes, 0, bytes.size)
                String(bytes)
            }
            val typeToken = object : TypeToken<List<ITAJsonModel>>() {}.type
            val list = parser.fromJson<List<ITAJsonModel>>(json, typeToken)
            val dbModels = list.map {
                IATAModel(
                    localId = it.id ?: -1,
                    continent = it.continent,
                    iata = it.iata,
                    iso = it.iso,
                    lat = it.lat?.toDoubleOrNull(),
                    lon = it.lon?.toDoubleOrNull(),
                    name = it.name?: "",
                    size = it.size,
                    isOpened = it.status == 1,
                    type = it.type
                )
            }

            repository.insertAll(*dbModels.toTypedArray())
            databaseStateLiveData.postValue(DatabaseState.READY)
        }
    }


    private val exceptionCoroutineContext = CoroutineExceptionHandler { _, t ->
        Timber.e(t)
        databaseStateLiveData.postValue(DatabaseState.ERROR)
    }


    enum class DatabaseState {
        EMPTY, //Indicate that we should start populate db
        READY, //Indicate that database ready
        ERROR //Something goes wrong
    }
}