package com.fly.whereita.di

import androidx.room.Room
import com.fly.whereita.FlyApplication
import com.fly.whereita.FlyDatabase
import com.fly.whereita.data.datasource.IATANetworkDataSource
import com.fly.whereita.data.network.core.NetworkClientBuilder
import com.fly.whereita.data.repository.ITARepository
import com.fly.whereita.domain.repository.IITARepository
import com.fly.whereita.utils.Config
import com.fly.whereita.viewmodel.IATADirectionsViewModel
import com.fly.whereita.viewmodel.IATAPricesViewModel
import com.fly.whereita.viewmodel.SearchViewModel
import com.fly.whereita.viewmodel.StartupViewModel
import com.google.gson.Gson
import kotlinx.coroutines.Dispatchers
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Yuriy Aizenberg on 22.04.2021.
 */
object DIModule {

    fun getModule() = module {
        single { Room.databaseBuilder(get(), FlyDatabase::class.java, FlyApplication.DB_NAME).fallbackToDestructiveMigration().build() }
        single { get<FlyDatabase>().flyDao() }

        single { NetworkClientBuilder.getApi(get()) }
        single { NetworkClientBuilder.getClient() }
        single { NetworkClientBuilder.getGson() }
        single { NetworkClientBuilder.getRetrofit(get(), get(), get()) }
        single { IATANetworkDataSource(get()) }
        single { Config.getTravelUrl() }

        single<IITARepository> { ITARepository(get(), get()) }
        viewModel { StartupViewModel(Dispatchers.IO, get(), Gson()) }
        viewModel { SearchViewModel(get()) }
        viewModel { IATADirectionsViewModel(get()) }
        viewModel { IATAPricesViewModel(get()) }

    }

}