package com.fly.whereita

import androidx.room.Database
import androidx.room.RoomDatabase
import com.fly.whereita.data.dao.ItaDao
import com.fly.whereita.data.entities.IATAEntity

/**
 * Created by Yuriy Aizenberg on 22.04.2021.
 */
@Database(entities = [IATAEntity::class], version = 2)
abstract class FlyDatabase: RoomDatabase() {

    abstract fun flyDao() : ItaDao

}