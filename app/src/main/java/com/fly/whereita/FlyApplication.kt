package com.fly.whereita

import android.app.Application
import androidx.room.Room
import com.fly.whereita.di.DIModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

/**
 * Created by Yuriy Aizenberg on 22.04.2021.
 */
class FlyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initializeLogger()
        initDependencyInjection()
    }

    private fun initializeLogger() {
        Timber.plant(Timber.DebugTree())
    }

    private fun initDependencyInjection() {
        startKoin {
            androidContext(this@FlyApplication)
            modules(
                listOf(
                    DIModule.getModule()
                )
            )
        }
    }

    companion object {
        const val DB_NAME = "fly_ita_db"
    }


}