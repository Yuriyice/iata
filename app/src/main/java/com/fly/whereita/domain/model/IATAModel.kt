package com.fly.whereita.domain.model

import android.os.Parcelable
import androidx.core.os.bundleOf
import kotlinx.android.parcel.Parcelize

/**
 * Created by Yuriy Aizenberg on 22.04.2021.
 */
@Parcelize
data class IATAModel(
    val localId: Long,
    val continent: String,
    val iata: String,
    val iso: String,
    val lat: Double?,
    val lon: Double?,
    val name: String,
    val size: String?,
    val isOpened: Boolean,
    val type: String
) : Parcelable{

    fun getUIName() = if (name.isNotBlank()) name else "Undefined"

    override fun toString(): String {
        return "ITAModel(continent='$continent', iata='$iata', iso='$iso', lat=$lat, lon=$lon, name='$name', size=$size, isOpened=$isOpened, type='$type')"
    }

    fun toBundle() = bundleOf(BUNDLE_KEY to this)

    companion object {
        const val BUNDLE_KEY = "iata"

    }
}