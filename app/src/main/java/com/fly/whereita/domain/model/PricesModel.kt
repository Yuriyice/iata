package com.fly.whereita.domain.model


import com.fly.whereita.domain.model.PricesModel.TripClass.Companion.nextId
import java.util.*
import java.util.concurrent.atomic.AtomicLong

data class PricesModel(
    val isActual: Boolean,
    val airline: String,
    val departDate: Date?,
    val destination: String,
    val distance: Int,
    val gate: String,
    val numberOfChanges: Int,
    val returnDate: Date?,
    val showToAffiliates: Boolean,
    val tripClass: TripClass,
    val value: Double
) {

    val internalId = nextId()

    enum class TripClass(val serverValue: Int) {
        STANDARD(0),
        BUSINESS(1);

        companion object {
            fun find(value: Int) = if (value == STANDARD.serverValue) STANDARD else BUSINESS
            private val counter = AtomicLong(0)

            fun nextId() = counter.incrementAndGet()


        }
    }
}