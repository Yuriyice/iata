package com.fly.whereita.domain.model

import android.os.Parcelable
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.parcel.Parcelize
import kotlin.math.ln

/**
 * Created by Yuriy Aizenberg on 25.04.2021.
 */
@Parcelize
class Coordinates(private val lat: Double?, private val lon: Double?) : Parcelable{

    fun getLatLng(): LatLng? {
        if (lat != null && lon != null) return LatLng(lat, lon)
        return null
    }

    companion object {
        //Be careful. API returns array with reversed data, like [lon, lat]
        fun build(list: List<Double>?) = Coordinates(list?.getOrNull(1), list?.getOrNull(0))
    }
}