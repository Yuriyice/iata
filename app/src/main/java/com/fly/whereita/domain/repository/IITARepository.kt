package com.fly.whereita.domain.repository

import com.fly.whereita.domain.model.IATADirectionsModel
import com.fly.whereita.domain.model.IATAModel
import com.fly.whereita.domain.model.PricesModel

/**
 * Created by Yuriy Aizenberg on 22.04.2021.
 */
interface IITARepository {

    suspend fun count(): Int

    suspend fun all() : List<IATAModel>

    suspend fun insertAll(vararg model: IATAModel)

    suspend fun findByItaOrName(query: String, limit: Int): List<IATAModel>

    suspend fun findSupportedDirectionsForIATA(iata: String) : IATADirectionsModel

    suspend fun findPrices(iata: String) : List<PricesModel>
}