package com.fly.whereita.domain.model

/**
 * Created by Yuriy Aizenberg on 25.04.2021.
 */
class IATADirectionsModel(val origin: OriginIATAModel, val directions: List<DirectionModel>)