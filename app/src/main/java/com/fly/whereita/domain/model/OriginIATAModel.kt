package com.fly.whereita.domain.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Yuriy Aizenberg on 25.04.2021.
 */
@Parcelize
class OriginIATAModel(
    val iata: String,
    val name: String,
    val country: String,
    val coordinates: Coordinates
) : Parcelable