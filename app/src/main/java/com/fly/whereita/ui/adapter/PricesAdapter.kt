package com.fly.whereita.ui.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fly.whereita.R
import com.fly.whereita.databinding.ItemPriceBinding
import com.fly.whereita.domain.model.PricesModel
import com.fly.whereita.utils.layoutInflater
import com.fly.whereita.utils.toAmountString
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Yuriy Aizenberg on 26.04.2021.
 */
class PricesAdapter : ListAdapter<PricesModel, PricesAdapter.PriceViewHolder>(diffUtils) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PriceViewHolder {
        return PriceViewHolder(
            parent.context.layoutInflater().inflate(R.layout.item_price, parent, false)
        )
    }

    override fun onBindViewHolder(holder: PriceViewHolder, position: Int) {
        val model = getItem(position)
        val context = holder.binding.root.context
        val formatter = SimpleDateFormat("dd MMMM, yyyy", Locale.getDefault())
        with(holder.binding) {
            txtPriceValue.text = context.getString(R.string.price_placeholder, model.value.toAmountString())
            val departDate = model.departDate
            if (departDate != null) {
                txtDepartValue.text = context.getString(
                    R.string.depart_date_placeholder,
                    formatter.format(departDate)
                )
            } else {
                txtDepartValue.setText(R.string.depart_date_unknown)
            }

            val returnDate = model.returnDate
            if (returnDate != null) {
                txtReturnValue.text = context.getString(
                    R.string.return_date_placeholder,
                    formatter.format(returnDate)
                )
            } else {
                txtReturnValue.setText(R.string.return_date_unknown)
            }
            txtTripType.text =
                context.getString(if (model.tripClass == PricesModel.TripClass.STANDARD) R.string.trip_type_standard else R.string.trip_type_business)
            txtAirlinesAndGate.text = "${model.airline}, ${model.gate}"
        }
    }


    class PriceViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemPriceBinding.bind(itemView)
    }


    companion object {
        val diffUtils = object : DiffUtil.ItemCallback<PricesModel>() {
            override fun areItemsTheSame(oldItem: PricesModel, newItem: PricesModel): Boolean {
                return oldItem.internalId == newItem.internalId
            }

            override fun areContentsTheSame(oldItem: PricesModel, newItem: PricesModel): Boolean {
                return oldItem.internalId == newItem.internalId
            }

        }
    }
}