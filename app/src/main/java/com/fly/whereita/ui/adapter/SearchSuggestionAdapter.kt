package com.fly.whereita.ui.adapter

import android.content.Context
import android.text.SpannableString
import android.text.style.BackgroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import androidx.core.content.ContextCompat
import com.fly.whereita.R
import com.fly.whereita.domain.model.IATAModel
import com.fly.whereita.utils.instanceIf
import kotlinx.android.synthetic.main.item_iata_sugggestion.view.*

/**
 * Created by Yuriy Aizenberg on 24.04.2021.
 */
class SearchSuggestionAdapter(context: Context, private val callback: IITAFilteringBridge) :
    BaseAdapter(), Filterable {

    private val datasource = ArrayList<IATAModel>()
    private var query = ""
    private val layoutInflater = LayoutInflater.from(context)
    private val highlightColor = ContextCompat.getColor(context, R.color.highlightSuggestionColor)

    fun setData(data: List<IATAModel>, query: String) {
        datasource.clear()
        datasource.addAll(data)
        this.query = query
        notifyDataSetChanged()
    }

    override fun getCount() = datasource.size

    override fun getItem(position: Int) = datasource[position]

    override fun getItemId(position: Int) = position.toLong()

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view =
            convertView ?: layoutInflater.inflate(R.layout.item_iata_sugggestion, parent, false)
        val model = getItem(position)
        val iataCodeSpannableString = SpannableString(model.iata)
        val iataNameSpannableString = SpannableString(model.name)

        /**Colorizer*/


        var queryParams = query.split(" ")
        var indexOfSearch = queryParams.map {
            model.iata.indexOf(it, ignoreCase = true) to it
        }

        indexOfSearch.forEach {
            if (it.first != -1) {
                iataCodeSpannableString.setSpan(
                    BackgroundColorSpan(highlightColor),
                    it.first,
                    it.first + it.second.length,
                    SpannableString.SPAN_INCLUSIVE_INCLUSIVE
                )
            }
        }
        view.txtIataCode.text = iataCodeSpannableString

        queryParams = query.split(" ")
        indexOfSearch = queryParams.map {
            model.name.indexOf(it, ignoreCase = true) to it
        }

        indexOfSearch.forEach {
            if (it.first != -1) {
                iataNameSpannableString.setSpan(
                    BackgroundColorSpan(highlightColor),
                    it.first,
                    it.first + it.second.length,
                    SpannableString.SPAN_INCLUSIVE_INCLUSIVE
                )
            }
        }
        view.txtIataName.text = iataNameSpannableString
        return view
    }





    override fun getFilter(): Filter {
        return object : Filter() {

            override fun convertResultToString(resultValue: Any?): CharSequence {
                return resultValue?.instanceIf<IATAModel>()?.iata ?: ""
            }

            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val result = FilterResults()
                val response = callback.performFiltering(constraint)
                result.values = response
                result.count = response.data.size
                return result
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                val result = results?.values?.instanceIf<WrapperResult>() ?: return
                val currentText = constraint?.toString() ?: ""
                if (result.requestedITA.equals(currentText, true)) {
                    setData(result.data, result.requestedITA)
                }
            }

        }
    }

    interface IITAFilteringBridge {
        fun performFiltering(constraints: CharSequence?): WrapperResult
    }

    class WrapperResult(val requestedITA: String, val data: List<IATAModel>)
}