package com.fly.whereita.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.DiffUtil.*
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.fly.whereita.R
import com.fly.whereita.databinding.ItemDestinationBinding
import com.fly.whereita.domain.model.DirectionModel
import com.fly.whereita.utils.*
import com.google.android.gms.maps.model.LatLng

/**
 * Created by Yuriy Aizenberg on 25.04.2021.
 */
class DestinationDetailsAdapter(private val startPoint: LatLng?, private val onItemClick: (model: DirectionModel) -> Unit) :
    ListAdapter<DirectionModel, DestinationDetailsAdapter.DestinationViewHolder>(
        diffUtil
    ) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DestinationViewHolder {
        return DestinationViewHolder(
            parent.context.layoutInflater().inflate(R.layout.item_destination, parent, false)
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: DestinationViewHolder, position: Int) {
        val model = getItem(position)
        val binding = holder.binding
        binding.root.setSingleClickListener {
            onItemClick.invoke(model)
        }
        val destinationCoordinates = model.coorindates.getLatLng()
        if (destinationCoordinates != null && startPoint != null) {
            val distance = destinationCoordinates.distanceToInMeters(startPoint)
            binding.txtDestinationDistance.text = binding.txtDestinationDetails.context.getString(R.string.distance_placeholder,
                distance.formatToKmOrMeter(
                    binding.root.context,
                    R.string.distance_in_m_placeholder,
                    R.string.distance_in_km_placeholder
                ))
            binding.txtDestinationDistance.show()
        } else {
            binding.txtDestinationDistance.hide()
        }
        binding.txtDestinationDetails.text =
            "${model.iata}, ${model.countryName}, ${model.name}"
        if (model.isDirect) {
            binding.txtDestinationDirectValue.setText(R.string.label_direct_route)
        } else {
            binding.txtDestinationDirectValue.setText(R.string.label_non_direct_route)
        }
    }


    class DestinationViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemDestinationBinding.bind(itemView)
    }

    companion object {
        val diffUtil = object : ItemCallback<DirectionModel>() {
            override fun areItemsTheSame(
                oldItem: DirectionModel,
                newItem: DirectionModel
            ): Boolean {
                return oldItem.iata.equals(newItem.iata, true)
            }

            override fun areContentsTheSame(
                oldItem: DirectionModel,
                newItem: DirectionModel
            ): Boolean {
                return oldItem.iata.equals(newItem.iata, true)
            }

        }
    }

}