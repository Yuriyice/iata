package com.fly.whereita.ui.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import com.fly.whereita.R
import com.fly.whereita.ui.utils.DialogCreator
import com.fly.whereita.viewmodel.StartupViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit

class SplashActivity : AppCompatActivity() {

    private val viewModel: StartupViewModel by viewModel()
    private val mainScreenHandler = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val startupTime = System.currentTimeMillis()
        viewModel.databaseStateEmitter.observe(this, {
            when (it!!) {
                StartupViewModel.DatabaseState.EMPTY -> {
                    viewModel.populateDatabase(this)
                }
                StartupViewModel.DatabaseState.READY -> {
                    openMain(startupTime)
                }
                StartupViewModel.DatabaseState.ERROR -> {
                    onError()
                }
            }
        })
        viewModel.checkDatabaseState()
    }

    private fun onError() {
        if (isDestroyed.not()) {
            DialogCreator.makeDialog(
                context = this,
                title = getString(R.string.dialog_default_error_title),
                message = getString(R.string.dialog_database_populate_error_description),
                actionButton = DialogCreator.ButtonData(
                    text = getString(R.string.dialog_default_action_button)
                ) {
                    it.dismiss()
                }
            ).show()
        }
    }

    private fun openMain(preparationStartedAt: Long) {
        val deltaTime = System.currentTimeMillis() - preparationStartedAt
        val timeThreshold =
            MINIMUM_SPLASH_SCREEN_DURATION_MS.coerceAtLeast(deltaTime) //Avoid splashscreen blinking
        var handlerTimeout = timeThreshold - deltaTime
        if (handlerTimeout < 0) handlerTimeout = 0
        mainScreenHandler.postDelayed({
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        }, handlerTimeout)

    }

    override fun onDestroy() {
        mainScreenHandler.removeCallbacksAndMessages(null)
        super.onDestroy()
    }

    companion object {
        private val MINIMUM_SPLASH_SCREEN_DURATION_MS = TimeUnit.SECONDS.toMillis(2)
    }
}