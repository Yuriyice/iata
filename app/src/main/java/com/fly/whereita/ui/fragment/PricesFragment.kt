package com.fly.whereita.ui.fragment

import android.os.Bundle
import android.view.View
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.fly.whereita.R
import com.fly.whereita.databinding.FragmentPricesBinding
import com.fly.whereita.domain.model.DirectionModel
import com.fly.whereita.domain.model.OriginIATAModel
import com.fly.whereita.domain.model.PricesModel
import com.fly.whereita.ui.adapter.PricesAdapter
import com.fly.whereita.ui.utils.DialogCreator
import com.fly.whereita.utils.hide
import com.fly.whereita.utils.show
import com.fly.whereita.viewmodel.IATAPricesViewModel
import com.fly.whereita.viewmodel.Outcome
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import org.koin.android.ext.android.inject
import java.util.*


/**
 * Created by Yuriy Aizenberg on 26.04.2021.
 */
class PricesFragment : Fragment(R.layout.fragment_prices) {

    private val viewModel: IATAPricesViewModel by inject()
    private lateinit var binding: FragmentPricesBinding
    private lateinit var directionModel: DirectionModel
    private lateinit var originModel: OriginIATAModel


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentPricesBinding.bind(view)
        directionModel = requireArguments().getParcelable(BUNDLE_KEY_DIRECTION)!!
        originModel = requireArguments().getParcelable(BUNDLE_KEY_ORIGIN)!!
        val adapter = PricesAdapter()
        binding.mapView.onCreate(savedInstanceState)
        binding.mapView.getMapAsync {
            onMapReady(it)
        }
        binding.rvPrices.layoutManager = LinearLayoutManager(requireContext())
        binding.rvPrices.adapter = adapter
        viewModel.pricesEmitter.observe(viewLifecycleOwner, { outcome ->
            binding.lottieLoadingPricesAnimationView.hide()
            onDataReady(outcome, adapter)
        })

    }

    private fun onDataReady(
        outcome: Outcome<List<PricesModel>>?,
        adapter: PricesAdapter
    ) {
        if (outcome is Outcome.Success) {
            adapter.submitList(outcome.data)
        } else {
            DialogCreator.makeDialog(
                context = requireContext(),
                title = getString(R.string.dialog_default_error_title),
                message = getString(R.string.dialog_default_error_description),
                actionButton = DialogCreator.ButtonData(
                    getString(R.string.dialog_default_action_button)
                ) {
                    it.dismiss()
                }
            ).show()
        }
    }

    private fun onMapReady(it: GoogleMap) {
        viewModel.findPrices(originModel.iata.toUpperCase(Locale.ENGLISH))
        binding.scrollContainer.show()
        binding.lottieLoadingContentAnimationView.hide()
        binding.lottieLoadingPricesAnimationView.show()
        binding.txtFromValue.text = originModel.name
        binding.txtToValue.text = directionModel.name

        val startPoint = originModel.coordinates.getLatLng()
        val endPoint = directionModel.coorindates.getLatLng()

        if (startPoint != null) {
            it.addMarker(MarkerOptions().position(startPoint))
        }
        if (endPoint != null) {
            it.addMarker(MarkerOptions().position(endPoint))
        }
        if (startPoint != null && endPoint != null) {
            calculateAndApplyZoom(startPoint, endPoint, it)
            it.addPolyline(PolylineOptions().add(startPoint, endPoint))
        }
    }

    override fun onResume() {
        super.onResume()
        binding.mapView.onResume()
    }

    override fun onStop() {
        binding.mapView.onStop()
        super.onStop()
    }

    override fun onPause() {
        binding.mapView.onPause()
        super.onPause()
    }

    override fun onStart() {
        super.onStart()
        binding.mapView.onStart()
    }

    override fun onDestroy() {
        binding.mapView.onDestroy()
        super.onDestroy()
    }

    override fun onLowMemory() {
        binding.mapView.onLowMemory()
        super.onLowMemory()
    }

    private fun calculateAndApplyZoom(firstPoint: LatLng, secondPoint: LatLng, map: GoogleMap) {
        binding.mapView.post {
            val builder: LatLngBounds.Builder = LatLngBounds.Builder()
            builder.include(firstPoint)
            builder.include(secondPoint)
            val bounds: LatLngBounds = builder.build()
            val padding = 100

            val cu = CameraUpdateFactory.newLatLngBounds(
                bounds,
                padding
            )
            map.moveCamera(cu)
        }
    }


    companion object {
        private const val BUNDLE_KEY_DIRECTION = "bkd"
        private const val BUNDLE_KEY_ORIGIN = "bko"

        fun args(directionModel: DirectionModel, originIATAModel: OriginIATAModel) = bundleOf(
            BUNDLE_KEY_DIRECTION to directionModel,
            BUNDLE_KEY_ORIGIN to originIATAModel
        )
    }


}