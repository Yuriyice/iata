package com.fly.whereita.ui.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.fly.whereita.domain.model.IATAModel
import androidx.recyclerview.widget.ListAdapter
import com.fly.whereita.R
import com.fly.whereita.databinding.ItemSearchResultBinding
import com.fly.whereita.utils.layoutInflater
import com.fly.whereita.utils.setSingleClickListener
import java.util.*

/**
 * Created by Yuriy Aizenberg on 25.04.2021.
 */
class SearchResultAdapter(private val onSingleItemClick: (item: IATAModel) -> Unit) : ListAdapter<IATAModel, SearchResultAdapter.ITAViewHolder>(diffUtil) {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ITAViewHolder {
        return ITAViewHolder(parent.context.layoutInflater().inflate(R.layout.item_search_result, parent, false))
    }

    override fun onBindViewHolder(holder: ITAViewHolder, position: Int) {
        val model = getItem(position)
        with(holder.binding) {
            txtIataCodeValue.text = model.iata.toUpperCase(Locale.getDefault())
            txtIataNameValue.text = model.getUIName()
            if (model.isOpened) {
                imgIataStatusValue.setImageResource(R.drawable.ic_opened)
            } else {
                imgIataStatusValue.setImageResource(R.drawable.ic_closed)
            }
            cardView.setSingleClickListener {
                onSingleItemClick.invoke(model)
            }
        }

    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }


    companion object {
        private val diffUtil = object : DiffUtil.ItemCallback<IATAModel>() {
            override fun areItemsTheSame(oldItem: IATAModel, newItem: IATAModel): Boolean {
                return oldItem.localId == newItem.localId
            }

            override fun areContentsTheSame(oldItem: IATAModel, newItem: IATAModel): Boolean {
                return oldItem.localId == newItem.localId
            }

        }
    }

    class ITAViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val binding = ItemSearchResultBinding.bind(itemView)
    }

}