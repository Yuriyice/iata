package com.fly.whereita.ui.utils

import android.app.Dialog
import android.content.Context
import android.graphics.Typeface
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.fly.whereita.databinding.DialogDefaultContentBinding
import com.fly.whereita.utils.hide
import com.fly.whereita.utils.layoutInflater
import com.fly.whereita.utils.px
import com.fly.whereita.utils.setSingleClickListener

/**
 * Created by Yuriy Aizenberg on 23.04.2021.
 */
object DialogCreator {

    fun makeDialog(
        context: Context,
        title: String,
        message: String,
        actionButton: ButtonData?
    ) : MaterialDialog {
        val materialDialog = MaterialDialog(context)

        val binding = DialogDefaultContentBinding.inflate(context.layoutInflater())

        binding.txtDialogTitle.text = title
        binding.txtDialogMessage.text = message
        if (actionButton != null) {
            binding.txtDialogAction.text = actionButton.text
            binding.btnDialogAction.setSingleClickListener {
                actionButton.onClick(materialDialog)
            }
        } else {
            binding.btnDialogAction.hide()
        }

        return materialDialog.show {
            cornerRadius(2.px.toFloat())
            customView(view = binding.root)
        }
    }

    class ButtonData(internal val text: String, internal val onClick: ((dialog: Dialog) -> Unit))
}