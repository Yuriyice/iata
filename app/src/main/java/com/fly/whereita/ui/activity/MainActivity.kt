package com.fly.whereita.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.fly.whereita.R
import com.fly.whereita.databinding.ActivityMainBinding
import com.fly.whereita.ui.fragment.SearchFragment

/**
 * Created by Yuriy Aizenberg on 23.04.2021.
 */
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        switchTo(SearchFragment::class.java, false)
    }

    fun switchTo(clazz: Class<out Fragment>, addToBackStack: Boolean = true, args: Bundle? = null) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.fragmentContainer, clazz, args)
        if (addToBackStack) {
            transaction.addToBackStack(clazz.simpleName)
        }
        transaction.commitAllowingStateLoss()
    }

}