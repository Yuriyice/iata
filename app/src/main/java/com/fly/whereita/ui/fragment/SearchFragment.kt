package com.fly.whereita.ui.fragment

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.fly.whereita.R
import com.fly.whereita.databinding.FragmentSearchBinding
import com.fly.whereita.domain.model.IATAModel
import com.fly.whereita.ui.activity.MainActivity
import com.fly.whereita.ui.adapter.SearchResultAdapter
import com.fly.whereita.ui.adapter.SearchSuggestionAdapter
import com.fly.whereita.utils.hideKeyboard
import com.fly.whereita.utils.instanceIf
import com.fly.whereita.utils.onSearch
import com.fly.whereita.viewmodel.Outcome
import com.fly.whereita.viewmodel.SearchViewModel
import org.koin.android.ext.android.inject
import timber.log.Timber

/**
 * Created by Yuriy Aizenberg on 24.04.2021.
 */
class SearchFragment : Fragment(R.layout.fragment_search),
    SearchSuggestionAdapter.IITAFilteringBridge {

    private lateinit var binding: FragmentSearchBinding
    private lateinit var searchSuggestionAdapter: SearchSuggestionAdapter
    private lateinit var searchListAdapter: SearchResultAdapter
    private val viewModel: SearchViewModel by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentSearchBinding.bind(view)
        searchSuggestionAdapter = SearchSuggestionAdapter(requireContext(), this)
        with(binding.edtIataSearch) {
            threshold = 2
            setAdapter(searchSuggestionAdapter)
            onSearch {
                performSearch(text.toString())
            }
            setOnItemClickListener { _, _, position, _ ->
                searchSuggestionAdapter.getItem(position).let {
                    performSearch(it.iata)
                    openIATADetailsScreen(it)
                }
            }
        }

        searchListAdapter = SearchResultAdapter {
            openIATADetailsScreen(it)
        }
        binding.rvIataList.layoutManager = LinearLayoutManager(requireContext())
        binding.rvIataList.adapter = searchListAdapter

        viewModel.searchItaEmitter.observe(viewLifecycleOwner, {
            if (it is Outcome.Success) {
                searchListAdapter.submitList(it.data)
            }
        })
    }

    private fun openIATADetailsScreen(data: IATAModel) {
        activity?.instanceIf<MainActivity>()
            ?.switchTo(DetailsFragment::class.java, true, data.toBundle())
    }


    private fun performSearch(query: String) {
        if (query.isNotBlank()) {
            searchSuggestionAdapter.setData(listOf(), "")
            hideKeyboard()
            viewModel.findItaByName(query)
        }
    }

    override fun performFiltering(constraints: CharSequence?): SearchSuggestionAdapter.WrapperResult {
        val query: String = constraints?.toString()?.trim() ?: ""
        val outcome = try {
            viewModel.findItaByNameSync(query, 6)
        } catch (e: Exception) {
            Outcome.failure(e)
        }
        return when (outcome) {
            is Outcome.Success -> {
                SearchSuggestionAdapter.WrapperResult(
                    outcome.data.originalQuery,
                    outcome.data.result
                )
            }
            is Outcome.Failure -> {
                Timber.e(outcome.e)
                SearchSuggestionAdapter.WrapperResult(query, listOf())
            }
        }
    }

}