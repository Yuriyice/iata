package com.fly.whereita.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.fly.whereita.R
import com.fly.whereita.databinding.FragmentIataDetailsBinding
import com.fly.whereita.domain.model.IATADirectionsModel
import com.fly.whereita.domain.model.IATAModel
import com.fly.whereita.ui.activity.MainActivity
import com.fly.whereita.ui.adapter.DestinationDetailsAdapter
import com.fly.whereita.ui.utils.DialogCreator
import com.fly.whereita.utils.hide
import com.fly.whereita.utils.instanceIf
import com.fly.whereita.utils.show
import com.fly.whereita.viewmodel.IATADirectionsViewModel
import com.fly.whereita.viewmodel.Outcome
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_iata_details.view.*
import org.koin.android.ext.android.inject

/**
 * Created by Yuriy Aizenberg on 25.04.2021.
 */
class DetailsFragment : Fragment(R.layout.fragment_iata_details) {

    private lateinit var binding: FragmentIataDetailsBinding
    private val viewModel: IATADirectionsViewModel by inject()
    private lateinit var model: IATAModel
    private var googleMap: GoogleMap? = null
    private lateinit var detailsAdapter: DestinationDetailsAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentIataDetailsBinding.bind(view)
        model = requireArguments().getParcelable(IATAModel.BUNDLE_KEY)!!
        binding.lottieLoadingDirectionAnimationView.show()
        binding.mapView.onCreate(savedInstanceState)
        binding.mapView.getMapAsync {
            googleMap = it
            requestData()
        }

        viewModel.searchIATADirectionsEmitter.observe(viewLifecycleOwner, { outcome ->
            onDataReady(outcome)
        })


    }

    private fun onDataReady(outcome: Outcome<IATADirectionsModel>?) {
        binding.lottieLoadingDirectionAnimationView.hide()
        if (outcome is Outcome.Success) {
            renderContent(outcome.data)
        } else {
            DialogCreator.makeDialog(
                context = requireContext(),
                title = getString(R.string.dialog_default_error_title),
                message = getString(R.string.dialog_default_error_description),
                actionButton = DialogCreator.ButtonData(getString(R.string.dialog_default_action_button)) { it.dismiss() }
            ).show()
        }
    }

    @SuppressLint("SetTextI18n")
    private fun renderContent(data: IATADirectionsModel) {
        with(binding) {
            if (data.directions.isEmpty()) {
                txtDirectionsNotFound.show()
            } else {
                txtDirectionsNotFound.hide()
            }
            val latLng = data.origin.coordinates.getLatLng()
            detailsAdapter = DestinationDetailsAdapter(latLng) {
                activity?.instanceIf<MainActivity>()?.switchTo(PricesFragment::class.java, true, PricesFragment.args(
                    it, data.origin
                ))
            }
            rvDirections.layoutManager = LinearLayoutManager(requireContext())
            rvDirections.adapter = detailsAdapter
            detailsAdapter.submitList(data.directions)
            scrollContainer.show()
            lottieLoadingContentAnimationView.hide()
            latLng?.let {
                googleMap?.addMarker(
                    MarkerOptions()
                        .position(it)
                )
                googleMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(it, 8f))
            }
            txtIataCodeValue.text = data.origin.iata
            txtIataNameValue.text = "${data.origin.name}, ${data.origin.country}"
            if (model.isOpened) {
                imgIataStatusValue.setImageResource(R.drawable.ic_opened)
            } else {
                imgIataStatusValue.setImageResource(R.drawable.ic_closed)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        binding.mapView.onResume()
    }

    override fun onPause() {
        binding.mapView.onPause()
        super.onPause()
    }

    override fun onStop() {
        binding.mapView.onStop()
        super.onStop()
    }

    override fun onLowMemory() {
        binding.mapView.onLowMemory()
        super.onLowMemory()
    }

    override fun onDestroy() {
        binding.mapView.onDestroy()
        super.onDestroy()
    }

    private fun requestData() {
        viewModel.searchDirectionsByIATACode(model.iata)
    }

}