package com.fly.whereita.utils

import android.view.KeyEvent
import android.widget.TextView

class ImeHandler(private val actionId: Int, private val onAction: () -> Unit) :
    TextView.OnEditorActionListener {
    override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
        return if (this.actionId == actionId) {
            this.onAction.invoke()
            true
        } else {
            false
        }
    }


}