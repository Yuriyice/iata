package com.fly.whereita.utils

import android.os.SystemClock
import android.view.View

private const val SINGLE_CLICK_INTERVAL = 500

class SingleClickListener(
    private val customClickInterval: Int = SINGLE_CLICK_INTERVAL,
    private val onSingleClick: (View) -> Unit
) : View.OnClickListener {

    private var lastTimeClicked = 0L

    override fun onClick(v: View) {
        if (SystemClock.elapsedRealtime() - lastTimeClicked > customClickInterval) {
            lastTimeClicked = SystemClock.elapsedRealtime()
            onSingleClick(v)
        }
    }
}