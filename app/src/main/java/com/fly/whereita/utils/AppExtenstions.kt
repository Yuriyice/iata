package com.fly.whereita.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Point
import android.location.Location
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.model.LatLng
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

/**
 * Created by Yuriy Aizenberg on 23.04.2021.
 */

fun Context.layoutInflater() = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

fun View.setSingleClickListener(onSingleClick: ((View) -> Unit)?) {
    val singleClickListener = SingleClickListener {
        onSingleClick?.invoke(it)
    }
    setOnClickListener(singleClickListener.takeIf { onSingleClick != null })
}

inline fun <reified T> Any.instanceIf(): T? {
    return if (this is T) {
        this
    } else {
        null
    }
}

fun View.show() {
    visibility = View.VISIBLE
}

fun View.hide() {
    visibility = View.GONE
}


fun Fragment.hideKeyboard() {
    view?.let { activity?.hideKeyboard(it) }
}


fun Context.hideKeyboard(view: View) {
    val inputMethodManager = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
}

fun EditText.onSearch(onAction: () -> Unit) {
    setOnEditorActionListener(ImeHandler(EditorInfo.IME_ACTION_SEARCH, onAction))
}

val Int.px: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun LatLng.distanceToInMeters(another: LatLng): Float {
    return FloatArray(2).apply {
        Location.distanceBetween(
            latitude,
            longitude,
            another.latitude,
            another.longitude,
            this
        )
    }[0]
}

fun Float.formatToKmOrMeter(
    context: Context,
    @StringRes metersRes: Int,
    @StringRes kmRes: Int
): String {
    return if (this > 1000) {
        val format = DecimalFormat("#.0").format(this / 1000f)
        if (format.endsWith(".0")) {
            context.getString(kmRes, format.replace(".0", ""))
        } else {
            context.getString(kmRes, format)
        }
    } else {
        context.getString(metersRes, "${this.toInt()}")
    }
}

fun Double.toAmountString(): String {
    val decimalFormatSymbols = DecimalFormatSymbols(Locale.getDefault())
    decimalFormatSymbols.groupingSeparator = ' '
    val formatter = DecimalFormat("#,###.##", decimalFormatSymbols)
    formatter.isGroupingUsed = true
    return formatter.format(this)
}
